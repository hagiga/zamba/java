# buildfile for the java language implementation of java.

GSC=gsc
GSC_OPTIONS=
CC_OPTIONS="-Wall "
LD_OPTIONS="-ltree-sitter -ltree-sitter-java"

OPTIONS= $(GSC_OPTIONS) -cc-options $(CC_OPTIONS) -ld-options $(LD_OPTIONS)

all:
	$(GSC) $(OPTIONS) -o $(BIN_DIR)/ts-java-binding.o1 ts-java-binding.scm
