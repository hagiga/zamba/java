;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2022 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(##namespace ("ts--java#"
parent-scopes
workspace-configuration
imported-namespaces
hover-information
filter-completions
))

;; propagate the associated filetypes.
(set! languages-by-suffix
  (append languages-by-suffix
          '((".java" . "java"))))

;; add the own language-object of ts
(set! known-languages
  (cons (cons "java" (ts--java#language-new))
        known-languages))

(define parent-scopes
  '(("block" local)
    ("constructor_body" local)
    ("class_body" member)
    ("interface_body" member)
    ("annotation_type_body" member)
    ("enum_body" member)
    ("program" member local)))

;; language dependend
(define keywords
  '("@interface" "abstract" "assert" "boolean" "break" "byte" "case" "catch" "char" "class"
    "continue" "default" "do" "double" "else" "enum" "extends" "false" "final" "finally" "float"
    "for" "if" "implements" "import" "instanceof" "int" "interface" "long" "native" "new"
    "null" "package" "private" "protected" "public" "return" "short" "static" "strictfp"
    "super" "switch" "synchronized" "this" "throw" "throws" "transient" "true" "try" "void"
    "volatile" "while"))

;; Offers the possiblity to language specific settings.
;; In a perfect world, this is empty.
;; The language standard library should be provided beforehand,
;; since it shall be declared by the user.
;;   Unfortunately it is difficult for the user to provide this.
;;   A server-specific file would be an aid, but this "floods"
;;   the filesystem.
;;
;; It would be convenient to pick up exisiting files.
(define (workspace-configuration params)
  (print-log "workspace-configuration: " params)
  ;(set! workspaces
  ;  (cons (make-lsp-workspace "file:///tmp/openjdk/" 'java-specific)
  ;        workspaces))
  ;; has to happen after initialize
  'maybe-decisions-on-stdlib)

(define (imported-namespaces uri doc)
  (let* ((ts--imports (read-file-relative-to (this-source-file) "queries/imports.ss"))
         (imports
           (fold (lambda (imp acc)
                   (let ((full (read-node (lsp-document-content doc) (cadr (member "name" imp)))))
                     (cons full acc)))
                 '("java.lang")
                 (ts--query-collect! doc ts--imports (ts--tree-root-node (lsp-document-tree doc))))))
    (fold (lambda (it acc) (if (member it acc) acc (cons it acc))) '() imports)))

(define (hover-information nodes)
  (print-log "hover-info: " nodes)
  (let* ((get-parent
           (lambda (node)
             (let ((parent (ts--node-parent node)))
               (if (and parent
                        (ts--node-named? parent))
                   parent
                   (if parent (get-parent parent))))))
         (node (ts--node-descendant-for-byte-range
                 (ts--tree-root-node
                   (lsp-document-tree (document-by-uri (caar nodes))))
                 (list-ref (car nodes) 5)
                 (list-ref (car nodes) 6)))
         (parent (get-parent node))
         (sexp (ts--node-sexp! parent))
         (query (with-exception-catcher
                  (lambda (ex) #f)
                  (lambda ()
                    (make-query
                      (document-by-uri (caar nodes))
                      (string-append
                        "((block_comment)+ @doc " sexp ")\n" )))))
         (info (call/cc (lambda (x) x))))
    (cond
      ((not query) "")
      ((procedure? info)
       (ts-query-execute!
         (ts--tree-root-node (lsp-document-tree (document-by-uri (caar nodes))))
         query
         (lambda x
           (info (string-append "```java"
                                (line-ending) (read-node (lsp-document-content (document-by-uri (caar nodes)))
                                                         (ts--query-capture-node (caaddr x)))
                                (line-ending) "```" (line-ending)))))
       #f)
      ((string? info) info)
      (else (error (string-append "whats this:" (object->string info)))))))

(define (filter-completions item-vector isIncomplete-box cursor uri doc word current-node)
  ;; one could add, filter and sort items.
  ;; by providing edit-commands, one can archive import functionalities...
  ;; the cursor is already placed at the parent-scope, but the current node is provided as well.
  ;; the document is loaded and the content can be retrieved.

  ;; Note that the box can be used to tell the client that no further completions can be provided.
  ;; it comes with #t as a default.

  ;; you should return the filtered vector
  (vector-append
    ;; append the keywords.
    (list->vector
      (fold
        (lambda (keyword acc)
          (if (>= (string-length keyword) strlen)
              (if (string=? (substring keyword 0 strlen) search-string)
                  (cons `(("label" . ,keyword)
                          ("kind" . ,completion-item-kind-keyword)
                          ("detail" . "Java Syntax")) acc)
                  acc)
              acc))
        '()
        keywords))
    item-vector))
