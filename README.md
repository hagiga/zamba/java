# language implementation of java for zamba

This repository assumes the be cloned by zamba, so it lies within the correct position on your filesystem.
The Makefile is mutated when called from zamba.

## zamba version

`0.8.0`

## information

This language implementation will recompute completions, if there are no completions found in the current scope.
This is due to the fact, that it makes no sense on the abstract syntax tree yet.


