;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(c-declare "#include <tree_sitter/api.h>
extern const TSLanguage *tree_sitter_java(void);")

(##namespace ("ts--java#"
language-new
))

;; the tree-sitter parser from the build library.
(define language-new (c-lambda () (pointer (struct "TSLanguage")) "___result_voidstar = (TSLanguage *) tree_sitter_java();"))
